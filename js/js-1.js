const buttons = document.getElementsByClassName("tabs-title");

for(let i = 0; i < buttons.length; i++) {
    buttons[i].addEventListener("click", toggleActive);
}

function toggleActive(event) {
    const buttons = document.getElementsByClassName("tabs-title");
    const dataTab = event.target.getAttribute('data-tab');
    const tabBody = document.getElementsByClassName('content-li');

    for (let i = 0; i < buttons.length; i++) {
        buttons[i].classList.remove("active");
    }

    for (let i = 0; i < tabBody.length; i++) {
        if (dataTab == i) {
            tabBody[i].style.display = 'inline';
        } else {
            tabBody[i].style.display = 'none'
        }
    }

    this.classList.toggle("active");
}

